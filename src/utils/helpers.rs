pub fn peal_off(buffer: &mut Vec<u8>, split_bytes: Vec<u8>) -> Vec<u8> {
    let mut new = buffer.clone();
    let mut positions = Vec::with_capacity(split_bytes.len());
    buffer.clear();

    for char in split_bytes {
        match new.iter().position(|x| x == &char) {
            Some(i) => {
                positions.push(i);
            }
            None => {}
        }
    }
    let pos = positions.iter().fold(new.len() ,|x, y| if x < *y {x} else {*y});
    buffer.extend(new.split_off(pos));
    if positions.len() > 0 {
        buffer.remove(0);
    }
    new
}

pub fn strip(buffer: &mut Vec<u8>) {
    let strip_chars = [b'\n', b' ', b'\r', b'\0', b'\t'];
    let mut run = true;
    while run {
        if buffer.len() == 0 {
            break;
        }
        run = false;
        for c in strip_chars.iter() {
            if *c == buffer[buffer.len() - 1] {
                run = true;
                buffer.pop();
                break;
            }
        }
    }
}