extern crate mysql;

use self::mysql::*;
use std::time::Duration;
use super::db::Database;

#[derive(Debug)]
pub struct Handler {
    handler: Pool,
    db: Option<String>,
}

#[derive(Debug)]
pub struct Attrs {
    pub ip: Option<String>,
    pub port: u16,
    pub user: Option<String>,
    pub password: Option<String>,
}

#[derive(Debug)]
pub struct Message {
    pub time: u64,
    pub state: u64,
    pub client: String,
    pub message: String,
}

#[derive(Debug)]
pub struct Legend {
    pub state: String,
    pub message: String,
}

impl Database for Handler {
    type Attributes = Attrs;
    type DbResult = Result<()>;
    type ConnResult = Result<Handler>;
    type Message = Message;
    type Legend = Legend;

    fn init(attributes: Self::Attributes) -> Result<Handler> {
        let mut options = OptsBuilder::default();
        options.ip_or_hostname(attributes.ip);
        options.tcp_port(attributes.port);
        options.user(attributes.user);
        options.pass(attributes.password);
        options.read_timeout(Some(Duration::from_secs(1)));
        options.write_timeout(Some(Duration::from_secs(1)));
        Pool::new(options).map(|handler| Handler { handler, db: None })
    }

    fn select_db(&mut self, db: &str) -> Self::DbResult {
        self.db = Some(String::from(db));
        match self.handler.get_conn() {
            Ok(mut conn) => { conn.query(format!("USE {}", db)).map(|_| ()) }
            Err(e) => { Err(e) }
        }
    }

    fn create_db(&mut self, db: &str) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => { conn.query(format!("CREATE DATABASE {}", db)).map(|_| ()) }
            Err(e) => { Err(e) }
        }
    }

    fn create_table_message(&mut self) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => {
                        conn.query(
                            format!(r"CREATE TABLE {}.message
                        (time BIGINT UNSIGNED NOT NULL, state BIGINT UNSIGNED NOT NULL, client TEXT NOT NULL, message TEXT NOT NULL);", name))
                            .map(|_| ())
                    }
                    None => {
                        conn.query(
                            format!(r"CREATE TABLE message
                        (time BIGINT UNSIGNED NOT NULL, state BIGINT UNSIGNED NOT NULL, client TEXT NOT NULL, message TEXT NOT NULL);"))
                            .map(|_| ())
                    }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn create_table_legend(&mut self) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => {
                        conn.query(
                            format!(r"CREATE TABLE {}.legend
                        (state BIGINT UNSIGNED NOT NULL UNIQUE, message TEXT NOT NULL);", name))
                            .map(|_| ())
                    }
                    None => {
                        conn.query(
                            format!(r"CREATE TABLE legend
                        (state BIGINT UNSIGNED NOT NULL UNIQUE, message TEXT NOT NULL);"))
                            .map(|_| ())
                    }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn drop_table_message(&mut self) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => { conn.query(format!(r"DROP TABLE {}.message;", name)).map(|_| ()) }
                    None => { conn.query(format!(r"DROP TABLE message;")).map(|_| ()) }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn drop_table_legend(&mut self) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => { conn.query(format!(r"DROP TABLE {}.legend;", name)).map(|_| ()) }
                    None => { conn.query(format!(r"DROP TABLE legend;")).map(|_| ()) }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn drop_db(&mut self, db: &str) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                conn.query(format!(r"DROP DATABASE {};", db)).map(|_| ())
            }
            Err(e) => { Err(e) }
        }
    }

    fn push_message(&mut self, msg: Self::Message) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => {
                        conn.query(format!(
                            r#"INSERT INTO {}.message
                            (time, state, client, message)
                            VALUES
                            ({}, {}, "{}", "{}")"#,
                            name,
                            msg.time,
                            msg.state,
                            msg.client,
                            msg.message)).map(|_| ())
                    }
                    None => {
                        conn.query(format!(
                            r#"INSERT INTO message
                            (time, state, client, message)
                            VALUES
                            ({}, {}, "{}", "{}")"#,
                            msg.time,
                            msg.state,
                            msg.client,
                            msg.message)).map(|_| ())
                    }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn push_messages(&mut self, msgs: &mut Vec<Self::Message>) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => {
                        let mut msg_iter = msgs.iter_mut();
                        let mut msg = msg_iter.next().unwrap();
                        let mut tmp = format!(
                            r#"INSERT INTO {}.message
                            (time, state, client, message)
                            VALUES ({}, {}, "{}", "{}")"#,
                            name,
                            msg.time,
                            msg.state,
                            msg.client,
                            msg.message);
                        for msg in msg_iter {
                            tmp = format!(r#"{} , ({}, {}, "{}", "{}")"#,
                                          tmp,
                                          msg.time,
                                          msg.state,
                                          msg.client,
                                          msg.message)
                        }
                        conn.query(tmp).map(|_| ())
                    }
                    None => {
                        let mut msg_iter = msgs.iter_mut();
                        let mut msg = msg_iter.next().unwrap();
                        let mut tmp = format!(
                            r#"INSERT INTO message
                            (time, state, client, message)
                            VALUES ({}, {}, "{}", "{}")"#,
                            msg.time,
                            msg.state,
                            msg.client,
                            msg.message);
                        for msg in msg_iter {
                            tmp = format!(r#"{} , ({}, {}, "{}", "{}")"#,
                                          tmp,
                                          msg.time,
                                          msg.state,
                                          msg.client,
                                          msg.message)
                        }
                        conn.query(tmp).map(|_| ())
                    }
                }
            }
            Err(e) => { Err(e) }
        }
    }

    fn push_legend(&mut self, msg: Self::Legend) -> Self::DbResult {
        match self.handler.get_conn() {
            Ok(mut conn) => {
                match self.db {
                    Some(ref name) => {
                        conn.query(format!(
                            r#"INSERT INTO {}.legend
                            (state, message)
                            VALUES
                            ({}, "{}")"#,
                            name,
                            msg.state,
                            msg.message)).map(|_| ())
                    }
                    None => {
                        conn.query(format!(
                            r#"INSERT INTO legend
                            (state, message)
                            VALUES
                            ({}, "{}")"#,
                            msg.state,
                            msg.message)).map(|_| ())
                    }
                }
            }
            Err(e) => { Err(e) }
        }
    }
}